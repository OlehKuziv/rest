namespace Catalog.DAL.Interfaces;

public interface IEntity<TKey>
{
    TKey Id { get; }
}