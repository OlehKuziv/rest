using Catalog.DAL.Entities;

namespace Catalog.DAL.Interfaces;

public interface IServiceRepository:IRepository<Service,long>
{
}