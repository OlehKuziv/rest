using System.Linq.Expressions;
using Catalog.DAL.Models;

namespace Catalog.DAL.Interfaces;

public interface IRepository<TEntity, in TKey> where TEntity:class, IEntity<TKey>
{
    Task<PagingResponse<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression, int limit, int offset, CancellationToken cancellationToken);
    Task<PagingResponse<TEntity>> GetAsync(int limit, int offset, CancellationToken cancellationToken);
    TEntity Insert(TEntity item);
    TEntity Update(TEntity item);
    bool Remove(TEntity item);
    Task<bool> RemoveAsync(TKey key);
    IUnitOfWork UnitOfWork { get; }
}