using Catalog.DAL.Entities;

namespace Catalog.DAL.Interfaces;

public interface IServiceCategoryRepository:IRepository<ServiceCategory, long>
{
    
}