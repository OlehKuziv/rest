using Catalog.DAL.DbContextConfiguration;
using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;

namespace Catalog.DAL.Repositories;

public class ServiceRepository:BaseRepository<Service,long>,IServiceRepository
{
    public ServiceRepository(CatalogDbContext context) : base(context)
    {
        UnitOfWork = context;
    }
    public override IUnitOfWork UnitOfWork { get; protected set; }
}