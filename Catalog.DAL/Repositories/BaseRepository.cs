using System.Linq.Expressions;
using Catalog.DAL.Interfaces;
using Catalog.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Catalog.DAL.Repositories;

public abstract class BaseRepository<TEntity, TKey>:IRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        public abstract IUnitOfWork UnitOfWork { get; protected set; }

        protected readonly DbContext _dbContext;
        protected readonly DbSet<TEntity> _entities;

        protected BaseRepository(DbContext context)
        {
            _dbContext = context;
            _entities = _dbContext.Set<TEntity>();
        }

        public virtual Task<PagingResponse<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression,int limit, int offset,CancellationToken cancellationToken)
        {
            return PaginateAsync(_entities.Where(expression), limit, offset, cancellationToken);
        }
        
        public virtual Task<PagingResponse<TEntity>> GetAsync(int limit, int offset, CancellationToken cancellationToken)
        {
            return PaginateAsync(_entities, limit, offset, cancellationToken);
        }

        public  virtual TEntity Insert(TEntity item)
        {
           return _entities.Add(item).Entity;
        }

        public virtual TEntity Update(TEntity item)
        {
            return _entities.Update(item).Entity;
        }

        public virtual bool Remove(TEntity item)
        {
            var result = _entities.Remove(item);
            return result.State == EntityState.Deleted;
        }

        public virtual async Task<bool> RemoveAsync(TKey key)
        {
            var item = await _entities.FindAsync(key);
            if (item == null)
                return false;

            var result = _entities.Remove(item);
            return result.State == EntityState.Deleted;
        }
        protected async Task<PagingResponse<TEntity>> PaginateAsync(IQueryable<TEntity> source, int limit, int offset, CancellationToken cancellationToken) 
        {
            var count = await source.AsNoTracking().CountAsync(cancellationToken);
            var paginated = await source.AsNoTracking().Skip(offset * limit ).Take(limit).ToArrayAsync(cancellationToken);
            return new PagingResponse<TEntity>(paginated, count);
        }
    }