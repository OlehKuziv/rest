using System.Linq.Expressions;
using Catalog.DAL.DbContextConfiguration;
using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;
using Catalog.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Catalog.DAL.Repositories;

public class ServiceCategoryRepository:BaseRepository<ServiceCategory,long>, IServiceCategoryRepository
{
    public ServiceCategoryRepository(CatalogDbContext context) : base(context)
    {
        UnitOfWork = context;
    }
    public override Task<PagingResponse<ServiceCategory>> GetAsync(int limit, int offset,CancellationToken cancellationToken)
    {
        return PaginateAsync(_entities.Include(e=>e.Services), limit, offset, cancellationToken);
    }
    public override IUnitOfWork UnitOfWork { get; protected set; }
}