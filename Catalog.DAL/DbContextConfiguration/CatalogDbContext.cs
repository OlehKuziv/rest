using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Catalog.DAL.DbContextConfiguration;

public class CatalogDbContext:DbContext, IUnitOfWork
{
    public DbSet<Service> Services { get; set; }
    public DbSet<ServiceCategory> ServiceCategories { get; set; }
    
    public CatalogDbContext(DbContextOptions<CatalogDbContext> options):base(options){}

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfiguration(new ServiceConfiguration());
        builder.ApplyConfiguration(new ServiceCategoryConfiguration());
    }
}