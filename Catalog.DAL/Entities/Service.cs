using Catalog.DAL.Interfaces;

namespace Catalog.DAL.Entities;

public class Service:IEntity<long>
{
    public long Id { get; set; }
    public string ServiceName { get; set; }
    public string Description { get; set; }
    public decimal BasePrice { get; set; }
    public long ServiceCategoryId { get; set; }
    //public long ImageId{get;set;}
    public virtual ServiceCategory ServiceCategory { get; set; }

    //added
    //public virtual ICollection<EmployeeService> EmployeeServices { get; set; }
    //public virtual ICollection<Order> Orders { get; set; }
}