using Catalog.DAL.Interfaces;

namespace Catalog.DAL.Entities;

public class ServiceCategory:IEntity<long>
{
    public long Id { get; set; }
    public string Description { get; set; }
    public long? ImageId { get; set; }
    public string Name { get; set; }

    public virtual ICollection<Service> Services { get; set; }
}