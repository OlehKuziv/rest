﻿using Catalog.DAL.DbContextConfiguration;

namespace Catalog;

public class Program
{
      public static async Task Main(string[] args)
      {
          var host = CreateHostBuilder(args).Build();
          await host.MigrateContextAsync<CatalogDbContext>();
          host.Run();
      }

     public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
}