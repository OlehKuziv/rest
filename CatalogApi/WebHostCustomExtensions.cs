using Microsoft.EntityFrameworkCore;

namespace Catalog;

public static class WebHostCustomExtensions
{
    public static async Task<IHost> MigrateContextAsync<TDbContext>(this IHost host) where TDbContext:DbContext
    {
        using (var scope = host.Services.CreateScope())
        {
            var serviceProvider = scope.ServiceProvider;
            var logger = serviceProvider.GetRequiredService<ILogger<TDbContext>>();
            var context = serviceProvider.GetRequiredService<TDbContext>();
            try
            {
                logger.LogInformation("Migrating database associated with context {DbContextName}", typeof(TDbContext).Name);
                await context.Database.MigrateAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred while migrating the database used on context {DbContextName}", typeof(TDbContext).Name);
            }

            return host;
        }
    }
}