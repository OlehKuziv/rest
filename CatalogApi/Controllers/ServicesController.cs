using System.Net;
using Catalog.BLL.Interfaces;
using Catalog.BLL.Models;
using Catalog.DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.Controllers;

    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController:ControllerBase
    {
        private readonly IServicesManager _servicesManager;
        
        public ServicesController(IServicesManager servicesManager)
        {
            _servicesManager = servicesManager;
        }
        
        [ProducesResponseType(typeof(PagingResponse<ServiceDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<ActionResult<PagingResponse<ServiceDto>>> GetAsync([FromQuery]PagingQuery pagingQuery,[FromQuery] IEnumerable<long> categoryIds)
        {
            var result = await _servicesManager.GetAsync(pagingQuery, categoryIds, HttpContext.RequestAborted);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(ServiceDto),(int) HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]ServicePostDto servicePostDto)
        {
            var result = await _servicesManager.PostAsync(servicePostDto);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(ServiceDto),(int) HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPut]
        public async Task<IActionResult>PutAsync([FromBody]ServicePutDto servicePostDto)
        {
            var result = await _servicesManager.UpdateAsync(servicePostDto);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(bool),(int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] long id)
        {
            var result = await _servicesManager.DeleteAsync(id);
            if (!result)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }