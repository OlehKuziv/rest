using System.Net;
using Catalog.BLL.Interfaces;
using Catalog.BLL.Models;
using Catalog.DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.Controllers;


[Route("api/[controller]")]
    [ApiController]
    public class CategoriesController:ControllerBase
    {
        private readonly ICategoryManager _categoryManager;
        
        public CategoriesController(ICategoryManager categoryManager)
        {
            _categoryManager = categoryManager;
        }
        
        [ProducesResponseType(typeof(PagingResponse<CategoryDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<ActionResult<PagingResponse<CategoryDto>>> GetAsync([FromQuery]PagingQuery pagingQuery)
        {
            var result = await _categoryManager.GetAsync(pagingQuery, HttpContext.RequestAborted);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(CategoryDto),(int) HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]CategoryPostDto categoryPostDto)
        {
            var result = await _categoryManager.PostAsync(categoryPostDto);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(CategoryDto),(int) HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPut]
        public async Task<IActionResult>PutAsync([FromBody]CategoryPutDto categoryPostDto)
        {
            var result = await _categoryManager.UpdateAsync(categoryPostDto);
            return Ok(result);
        }
        
        [ProducesResponseType(typeof(bool),(int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] long id)
        {
            var result = await _categoryManager.DeleteAsync(id);
            if (!result)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }