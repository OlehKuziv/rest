using System.Reflection;
using Catalog.BLL.Interfaces;
using Catalog.BLL.Models;
using Catalog.BLL.Services;
using Catalog.DAL.DbContextConfiguration;
using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;
using Catalog.DAL.Models;
using Catalog.DAL.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Catalog;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected IConfiguration Configuration { get; set; }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddScoped<IServiceCategoryRepository, ServiceCategoryRepository>();
        services.AddScoped<IServiceRepository, ServiceRepository>();
        services.AddScoped<ICategoryManager, CategoryManager>();
        services.AddScoped<IServicesManager, ServicesManager>();
        services.AddAutoMapper(typeof(ServicePostDto), typeof(Service), typeof(PagingResponse<Service>), typeof(PagingResponse<ServiceDto>), typeof(ServiceDto),
            typeof(PagingResponse<ServiceCategory>), typeof(PagingResponse<CategoryDto>), typeof(CategoryPostDto), typeof(ServiceCategory),typeof(CategoryDto));

        services.AddDbContext<CatalogDbContext>((sp,opt)=>
        {
            opt.UseSqlite("data source=.\\sqlexpress\\CatalogDb");
        });
        services.AddSwaggerDocument(c =>
        {
            c.GenerateEnumMappingDescription = true;
            c.Title = "JustPay API";
            c.Version = "latest";
            c.GenerateExamples = true;
        });
        services.AddControllers();
    }
    
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseOpenApi();
        app.UseSwaggerUi3(c =>
        {
            c.Path = "";
            c.EnableTryItOut = true;
            c.DocumentTitle = "JustPay API";
            c.DocExpansion = "list";
        });
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            endpoints.MapControllers();
        });
    }
}