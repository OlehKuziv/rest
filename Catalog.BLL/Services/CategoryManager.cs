using AutoMapper;
using Catalog.BLL.Interfaces;
using Catalog.BLL.Models;
using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;
using Catalog.DAL.Models;

namespace Catalog.BLL.Services;

public class CategoryManager:ICategoryManager
{
    private readonly IMapper _mapper;
    private readonly IServiceCategoryRepository _categoryRepository;

    public CategoryManager(IMapper mapper, IServiceCategoryRepository categoryRepository)
    {
        _mapper = mapper;
        _categoryRepository = categoryRepository;
    }

    public async Task<PagingResponse<CategoryDto>> GetAsync(PagingQuery pagingQuery, CancellationToken cancellationToken)
    {
        PagingResponse<ServiceCategory> pagingResponse = await _categoryRepository.GetAsync(pagingQuery.PageSize, pagingQuery.PageIndex, cancellationToken);
        return _mapper.Map<PagingResponse<ServiceCategory>, PagingResponse<CategoryDto>>(pagingResponse);
    }

    public async Task<CategoryDto> PostAsync(CategoryPostDto categoryPostDto)
    {
        var entity = _mapper.Map<CategoryPostDto, ServiceCategory>(categoryPostDto);
        var result =  _categoryRepository.Insert(entity);
        await _categoryRepository.UnitOfWork.SaveChangesAsync();
        var mapped = _mapper.Map<ServiceCategory, CategoryDto>(result);
        return mapped;
    }

    public async Task<CategoryDto> UpdateAsync(CategoryPutDto categoryPostDto)
    {
        var entity = _mapper.Map<CategoryPutDto, ServiceCategory>(categoryPostDto);
        var result =  _categoryRepository.Update(entity);
        await _categoryRepository.UnitOfWork.SaveChangesAsync();
        var mapped = _mapper.Map<ServiceCategory, CategoryDto>(result);
        return mapped;
    }

    public async Task<bool> DeleteAsync(long categoryId)
    {
        var result =  await _categoryRepository.RemoveAsync(categoryId);
        await _categoryRepository.UnitOfWork.SaveChangesAsync();
        return result;
    }
}