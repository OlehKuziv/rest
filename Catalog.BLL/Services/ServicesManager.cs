using System.Linq.Expressions;
using AutoMapper;
using Catalog.BLL.Interfaces;
using Catalog.BLL.Models;
using Catalog.DAL.Entities;
using Catalog.DAL.Interfaces;
using Catalog.DAL.Models;

namespace Catalog.BLL.Services;

public class ServicesManager:IServicesManager
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IMapper _mapper;
        private readonly IServiceCategoryRepository _categoryRepository;

        public ServicesManager(IServiceRepository serviceRepository,IServiceCategoryRepository categoryRepository, IMapper mapper)
        {
            _serviceRepository = serviceRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }
        public async Task<PagingResponse<ServiceDto>> GetAsync(PagingQuery pagingQuery, IEnumerable<long> categoryIds, CancellationToken cancellationToken)
        {
             Expression<Func<Service, bool>> predicate =(service)=>!categoryIds.Any() || categoryIds.Contains(service.ServiceCategoryId);
             PagingResponse<Service> pagingResponse = await _serviceRepository.GetAsync(predicate,pagingQuery.PageSize, pagingQuery.PageIndex, cancellationToken);
             return _mapper.Map<PagingResponse<Service>, PagingResponse<ServiceDto>>(pagingResponse);
        }
        

        public async Task<ServiceDto> UpdateAsync(ServicePutDto servicePostDto)
        {
            var entity = _mapper.Map<ServicePutDto, Service>(servicePostDto);
            var result =  _serviceRepository.Update(entity);
            await _serviceRepository.UnitOfWork.SaveChangesAsync();
            var mapped = _mapper.Map<Service, ServiceDto>(result);
            return mapped;
        }

        public async Task<ServiceDto> PostAsync(ServicePostDto servicePostDto)
        {
            var entity = _mapper.Map<ServicePostDto, Service>(servicePostDto);
            var categoryDto = await _categoryRepository.GetAsync(e => e.Id.Equals(entity.ServiceCategoryId), 1, 0, CancellationToken.None);
            if (categoryDto.Elements.Any())
            {
                // TODO insert or update
                var result =  _serviceRepository.Insert(entity);
                await _serviceRepository.UnitOfWork.SaveChangesAsync();
                var mapped = _mapper.Map<Service, ServiceDto>(result);
                return mapped;
            }
            return null;
        }

        public async Task<bool> DeleteAsync(long serviceId)
        {
            var result =  await _serviceRepository.RemoveAsync(serviceId);
            await _serviceRepository.UnitOfWork.SaveChangesAsync();
            return result;
        }
    }