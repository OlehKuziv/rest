using AutoMapper;
using Catalog.BLL.Models;
using Catalog.DAL.Entities;
using Catalog.DAL.Models;

namespace Catalog.BLL.Services;

public class AutoMapping:Profile
{
    public AutoMapping()
    {
        CreateMap<ServicePostDto, Service>();
        CreateMap<ServicePutDto, Service>();
        CreateMap<PagingResponse<Service>, PagingResponse<ServiceDto>>();
        CreateMap<Service, ServiceDto>();

        CreateMap<PagingResponse<ServiceCategory>, PagingResponse<CategoryDto>>();
        CreateMap<CategoryPostDto, ServiceCategory>();
        CreateMap<CategoryPutDto, ServiceCategory>();
        CreateMap<ServiceCategory, CategoryDto>();
    }
}