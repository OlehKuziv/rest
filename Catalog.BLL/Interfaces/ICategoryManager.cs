using Catalog.BLL.Models;
using Catalog.DAL.Models;

namespace Catalog.BLL.Interfaces;

public interface ICategoryManager
{
    Task<PagingResponse<CategoryDto>> GetAsync(PagingQuery pagingQuery, CancellationToken cancellationToken);
    Task<CategoryDto> PostAsync(CategoryPostDto categoryPostDto);
    Task<CategoryDto> UpdateAsync(CategoryPutDto categoryPostDto);
    Task<bool> DeleteAsync(long categoryId);
}