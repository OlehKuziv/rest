using Catalog.BLL.Models;
using Catalog.DAL.Models;

namespace Catalog.BLL.Interfaces;

public interface IServicesManager
{
    Task<PagingResponse<ServiceDto>> GetAsync(PagingQuery pagingQuery, IEnumerable<long> categoryIds, CancellationToken cancellationToken);
    Task<ServiceDto> PostAsync(ServicePostDto servicePostDto);
    Task<ServiceDto> UpdateAsync(ServicePutDto servicePostDto);
    Task<bool> DeleteAsync(long serviceId);
}