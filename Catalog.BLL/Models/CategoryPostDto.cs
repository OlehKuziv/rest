namespace Catalog.BLL.Models;

public class CategoryPostDto
{
    public string Description { get; set; }
    public long? ImageId { get; set; }
    public string Name { get; set; }
}