namespace Catalog.BLL.Models;

public class CategoryDto
{
    public long Id { get; set; }
    public string Description { get; set; }
    public long? ImageId { get; set; }
    public string Name { get; set; }
    public ICollection<ServiceDto> Services { get; set; }
}