namespace Catalog.BLL.Models;

public class CategoryPutDto:CategoryPostDto
{
    public long Id { get; set; }
}