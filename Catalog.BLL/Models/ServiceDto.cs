namespace Catalog.BLL.Models;

public class ServiceDto
{
    public long Id { get; set; }
    public string ServiceName { get; set; }
    public string Description { get; set; }
}