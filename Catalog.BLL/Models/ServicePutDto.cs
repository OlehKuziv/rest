namespace Catalog.BLL.Models;

public class ServicePutDto
{
    public long Id { get; set; }
    public decimal BasePrice { get; set; }
    public string ServiceName { get; set; }
    public string Description { get; set; }
}