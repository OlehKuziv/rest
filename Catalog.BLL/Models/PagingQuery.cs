namespace Catalog.BLL.Models;

public class PagingQuery
{
    private int? _pageSize;
    private int? _pageIndex;
    public int PageSize { get=>_pageSize??10;  set=>_pageSize=value; }
    public int PageIndex { get=>_pageIndex??0;  set=>_pageIndex=value; }
}