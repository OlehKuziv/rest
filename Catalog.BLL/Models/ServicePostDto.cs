namespace Catalog.BLL.Models;

public class ServicePostDto
{
    public long ServiceCategoryId { get; set; }
    public decimal BasePrice { get; set; }
    public string ServiceName { get; set; }
    public string Description { get; set; }
}